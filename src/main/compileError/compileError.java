package main.compileError;

import java.util.*;

public class compileError {
    public static HashMap<String, Integer> errorMap;

    public compileError () {
        errorMap = new LinkedHashMap<String, Integer>();
    }

    public static void sortByValue()
    {
        List<Map.Entry<String, Integer> > list =
                new LinkedList<Map.Entry<String, Integer> >(errorMap.entrySet());
        Collections.sort(list, new Comparator<Map.Entry<String, Integer> >() {
            public int compare(Map.Entry<String, Integer> o1,
                               Map.Entry<String, Integer> o2)
            {
                return (o1.getValue()).compareTo(o2.getValue());
            }
        });

        // put data from sorted list to hash map
        HashMap<String, Integer> temp = new LinkedHashMap<String, Integer>();
        for (Map.Entry<String, Integer> aa : list) {
            temp.put(aa.getKey(), aa.getValue());
        }
        errorMap = temp;
    }

    public void put(String errorName, int line) {
        errorMap.put(errorName, line);
    }

    public static HashMap<String, Integer> getErrorMap() {
        return errorMap;
    }

    public int getMapSize() {
        return errorMap.size();
    }
}




