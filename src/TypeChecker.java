import main.ast.node.Main;
import main.ast.node.Program;
import main.ast.node.declaration.ActorDeclaration;
import main.ast.node.declaration.ActorInstantiation;
import main.ast.node.declaration.VarDeclaration;
import main.ast.node.declaration.handler.HandlerDeclaration;
import main.ast.node.expression.*;
import main.ast.node.expression.values.BooleanValue;
import main.ast.node.expression.values.IntValue;
import main.ast.node.expression.values.StringValue;
import main.ast.node.statement.*;
import main.ast.type.Type;
import main.ast.type.actorType.ActorType;
import main.visitor.VisitorImpl;

import java.util.ArrayList;

public class TypeChecker extends VisitorImpl {

    protected void visitStatement( Statement stat )
    {
        if( stat == null )
            return;
        else if( stat instanceof MsgHandlerCall)
            this.visit( ( MsgHandlerCall ) stat );
        else if( stat instanceof Block)
            this.visit( ( Block ) stat );
        else if( stat instanceof Conditional)
            this.visit( ( Conditional ) stat );
        else if( stat instanceof For)
            this.visit( ( For ) stat );
        else if( stat instanceof Break )
            this.visit( ( Break ) stat );
        else if( stat instanceof Continue )
            this.visit( ( Continue ) stat );
        else if( stat instanceof Print )
            this.visit( ( Print ) stat );
        else if( stat instanceof Assign )
            this.visit( ( Assign ) stat );
    }

    protected void visitExpr( Expression expr )
    {
        if( expr == null )
            return;
        else if( expr instanceof UnaryExpression)
            this.visit( ( UnaryExpression ) expr );
        else if( expr instanceof BinaryExpression)
            this.visit( ( BinaryExpression ) expr );
        else if( expr instanceof ArrayCall)
            this.visit( ( ArrayCall ) expr );
        else if( expr instanceof ActorVarAccess)
            this.visit( ( ActorVarAccess ) expr );
        else if( expr instanceof Identifier )
            this.visit( ( Identifier ) expr );
        else if( expr instanceof Self )
            this.visit( ( Self ) expr );
        else if( expr instanceof Sender )
            this.visit( ( Sender ) expr );
        else if( expr instanceof BooleanValue)
            this.visit( ( BooleanValue ) expr );
        else if( expr instanceof IntValue)
            this.visit( ( IntValue ) expr );
        else if( expr instanceof StringValue)
            this.visit( ( StringValue ) expr );
    }

    ArrayList<String> actorNames = new ArrayList<String>();
    ArrayList<String> variableNames = new ArrayList<String>();
    ArrayList<String> knownActorNames = new ArrayList<String>();
    ArrayList<String> msgHandlerNames = new ArrayList<String>();
    ArrayList<String> actorVarNames = new ArrayList<String>();
    ArrayList<String> handlerArgNames = new ArrayList<String>();
    ArrayList<String> handlerLocalVarsNames = new ArrayList<String>();
    ArrayList<String> actorInstantiationNames = new ArrayList<String>();
    String currentActorName;
    int currentBlockNum = 0;
    boolean inLoop = false;

    public void addParentsVars(ArrayList<String> listVar, String parentName, String name){
        for (int i = 0; i < listVar.size(); i++)
            if (listVar.get(i).contains("_")){
                String[] namePart = listVar.get(i).split("_", 2);
                if (namePart[1] == parentName ) {
                    String addVar = namePart[0] + "_" + name;
                    listVar.add(addVar);
                }
            }
    }

    @Override
    public void visit(Program program) {

        if (program.getActors() != null) {
            for (int i = 0; i < program.getActors().size(); ++i) {
                currentActorName = program.getActors().get(i).getName().getName();
                program.getActors().get(i).accept(this);
                actorNames.add(currentActorName);
            }
        }
        if (program.getMain() != null)
            program.getMain().accept(this);
    }

    @Override
    public void visit(ActorDeclaration actorDeclaration) {

        if (actorDeclaration.getName() != null) {
            actorDeclaration.getName().accept(this);
        }

        if (actorDeclaration.getParentName() != null) {
            actorDeclaration.getParentName().accept(this);
            addParentsVars(knownActorNames,  actorDeclaration.getParentName().getName(), actorDeclaration.getName().getName());
            addParentsVars(actorVarNames,  actorDeclaration.getParentName().getName(), actorDeclaration.getName().getName());
            addParentsVars(msgHandlerNames,  actorDeclaration.getParentName().getName(), actorDeclaration.getName().getName());
            if (!actorNames.contains(actorDeclaration.getParentName().getName())) {
                String error_name = "Line:" + actorDeclaration.getName().getLine() +
                        ":actor " + actorDeclaration.getParentName().getName() + " is not declared";
                errors.put(error_name, actorDeclaration.getName().getLine());
            }
        }

        if (actorDeclaration.getKnownActors() != null) {
            for(int i=0; i<actorDeclaration.getKnownActors().size() ; ++i ) {
                actorDeclaration.getKnownActors().get(i).accept(this);
                knownActorNames.add(actorDeclaration.getKnownActors().get(i).getIdentifier().getName()+ "_" + currentActorName); //add even if the knwonactor doesnt exist?
            }
        }

        if (actorDeclaration.getActorVars() != null) {
            for(int i=0; i<actorDeclaration.getActorVars().size() ; ++i ) {
                actorDeclaration.getActorVars().get(i).accept(this);
                actorVarNames.add(actorDeclaration.getActorVars().get(i).getIdentifier().getName()+ "_" + currentActorName);
            }
        }

        if (actorDeclaration.getInitHandler() != null) {
            actorDeclaration.getInitHandler().accept(this);
            msgHandlerNames.add(actorDeclaration.getInitHandler().getName().getName() + "_" + currentActorName);

        }

        if (actorDeclaration.getMsgHandlers() != null) {
            for(int i=0; i<actorDeclaration.getMsgHandlers().size() ; ++i ) {
                actorDeclaration.getMsgHandlers().get(i).accept(this);
                msgHandlerNames.add(actorDeclaration.getMsgHandlers().get(i).getName().getName() + "_" + currentActorName);
            }
        }

    }

    @Override
    public void visit(HandlerDeclaration handlerDeclaration) {

        if (handlerDeclaration.getName() != null)
            handlerDeclaration.getName().accept(this);

        if (handlerDeclaration.getArgs() != null) {
            for(int i=0; i<handlerDeclaration.getArgs().size() ; ++i ) {
                handlerDeclaration.getArgs().get(i).accept(this);
                handlerArgNames.add(handlerDeclaration.getArgs().get(i).getIdentifier().getName() + "_" +
                        handlerDeclaration.getArgs().get(i).getType().toString() + "_" + handlerDeclaration.getName().getName() );
            }
        }

        if (handlerDeclaration.getLocalVars() != null) {
            for(int i=0; i<handlerDeclaration.getLocalVars().size() ; ++i ) {
                handlerDeclaration.getLocalVars().get(i).accept(this);
                if( handlerDeclaration.getLocalVars().get(i).getIdentifier().getName().equals("sender")
                        && handlerDeclaration.getName().getName().equals("initial")){
                    String error_name = "Line:"+handlerDeclaration.getLine()+
                            ": no sender in initial msghandler";
                    errors.put(error_name, handlerDeclaration.getLine());
                }
                handlerLocalVarsNames.add(handlerDeclaration.getLocalVars().get(i).getIdentifier().getName() + "_" +
                        handlerDeclaration.getLocalVars().get(i).getType().toString() + "_" + handlerDeclaration.getName().getName() );
            }
        }

        if (handlerDeclaration.getBody() != null) {
            for(int i=0; i<handlerDeclaration.getBody().size() ; ++i ) {
                handlerDeclaration.getBody().get(i).accept(this);
            }
        }
    }

    @Override
    public void visit(VarDeclaration varDeclaration) {
        if (varDeclaration.getIdentifier() != null) {
            varDeclaration.getIdentifier().accept(this);
            if(varDeclaration.getType() instanceof ActorType) {
                String actorType = ((ActorType) varDeclaration.getType()).getName().getName();
                if (!actorNames.contains(actorType)) {
                    String error_name = "Line:"+varDeclaration.getIdentifier().getLine()+
                            ":actor " + varDeclaration.getIdentifier().getName() + " is not declared";
                    errors.put(error_name, varDeclaration.getIdentifier().getLine());
                }
            }
            variableNames.add(varDeclaration.getIdentifier().getName()+ "_" + currentBlockNum);//check num to string
        }
    }

    @Override
    public void visit(Main mainActors) {//does it need anything

        if (mainActors.getMainActors() != null) {
            for(int i=0; i<mainActors.getMainActors().size() ; ++i ) {
                mainActors.getMainActors().get(i).accept(this);
            }
        }
    }

    @Override
    public void visit(ActorInstantiation actorInstantiation) {
        if (actorInstantiation.getIdentifier() != null) {
            actorInstantiation.getIdentifier().accept(this);
            if (actorInstantiation.getType() instanceof ActorType) {
                String actorType = ((ActorType) actorInstantiation.getType()).getName().getName();
                if (!actorNames.contains(actorType)) {
                    String error_name = "Line:" + actorInstantiation.getIdentifier().getLine() +
                            ":actor " + actorInstantiation.getIdentifier().getName() + " is not declared";
                    errors.put(error_name, actorInstantiation.getIdentifier().getLine());
                }
            }
        }
        if (actorInstantiation.getKnownActors() != null) { //check sequence and name
            for (int i = 0; i < actorInstantiation.getKnownActors().size(); ++i) {
                actorInstantiation.getKnownActors().get(i).accept(this);
            }
            if (actorInstantiation.getInitArgs() != null) {////check sequence and name
                for (int i = 0; i < actorInstantiation.getInitArgs().size(); ++i) {
                    actorInstantiation.getInitArgs().get(i).accept(this);
                }
            }
        }
    }
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        @Override
        public void visit(UnaryExpression unaryExpression) {

            if (unaryExpression.getUnaryOperator() != null)
                if (unaryExpression.getOperand() != null)
                    unaryExpression.getOperand().accept(this);
        }

        @Override
        public void visit(BinaryExpression binaryExpression) {

            if(binaryExpression.getLeft() != null)
                binaryExpression.getLeft().accept(this);
            if(binaryExpression.getBinaryOperator() != null)
                if(binaryExpression.getRight() != null)
                    binaryExpression.getRight().accept(this);
        }

        @Override
        public void visit(ArrayCall arrayCall) {

            if (arrayCall.getArrayInstance() != null)
                arrayCall.getArrayInstance().accept(this);
            if (arrayCall.getIndex() != null)
                arrayCall.getIndex().accept(this);
        }

        @Override
        public void visit(ActorVarAccess actorVarAccess) {

            if (actorVarAccess.getSelf() != null)
                actorVarAccess.getSelf().accept(this);

            if (actorVarAccess.getVariable() != null)
                actorVarAccess.getVariable().accept(this);
        }

        @Override
        public void visit(Identifier identifier) {//does it need?
        }

        @Override
        public void visit(Self self) {
        }

        @Override
        public void visit(Sender sender) {
        }

        @Override
        public void visit(BooleanValue value) {
        }

        @Override
        public void visit(IntValue value) {
        }

        @Override
        public void visit(StringValue value) {
        }

        @Override
        public void visit(Block block) {
            currentBlockNum++;
            if (block.getStatements() != null) {
                for(int i=0; i<block.getStatements().size() ; ++i ) {
                    block.getStatements().get(i).accept(this);
                }
            }
            currentBlockNum--;
        }

        @Override
        public void visit(Conditional conditional) {

            if (conditional.getExpression() != null)
                conditional.getExpression().accept(this);

            if (conditional.getThenBody() != null)
                conditional.getThenBody().accept(this);

            if (conditional.getElseBody() != null)
                conditional.getElseBody().accept(this);
        }

        @Override
        public void visit(For loop) {

            if (loop.getInitialize() != null)
                loop.getInitialize().accept(this);

            if (loop.getCondition() != null)
                loop.getCondition().accept(this);

            if (loop.getUpdate() != null)
                loop.getUpdate().accept(this);

            if (loop.getBody() != null)
                loop.getBody().accept(this);
        }

        @Override
        public void visit(Break breakLoop) {
            if (!inLoop){
                String error_name = "break statement not within loop";
                errors.put(error_name, breakLoop.getLine());

            }
        }

        @Override
        public void visit(Continue continueLoop) {
            if (!inLoop){
                String error_name = "continue statement not within loop";
                errors.put(error_name, continueLoop.getLine());

            }
        }

        @Override
        public void visit(MsgHandlerCall msgHandlerCall) {

            if (msgHandlerCall.getInstance() != null)
                msgHandlerCall.getInstance().accept(this);

            if (msgHandlerCall.getMsgHandlerName() != null)
                msgHandlerCall.getMsgHandlerName().accept(this);

            String msgHandlerName = msgHandlerCall.getMsgHandlerName().getName();
            Type type = msgHandlerCall.getInstance().getType();
            if (type instanceof ActorType) {
                String actorName = ((ActorType) type).getName().getName();
                if (!actorNames.contains(actorName)) {
                    String error_name = "Line:" + msgHandlerCall.getLine() +
                            ":variable " + actorName + " is not callable";
                    errors.put(error_name, msgHandlerCall.getLine());
                }

                if (!(msgHandlerNames).contains(msgHandlerName + "_" + actorName)) {
                    String error_name = "Line:" + msgHandlerCall.getLine() +
                            ":there is no msghandler name " + msgHandlerName + " in actor " + actorName;
                    errors.put(error_name, msgHandlerCall.getLine());
                }
            }
            if (msgHandlerCall.getArgs() != null) {
                for (Expression expression : msgHandlerCall.getArgs()) {
                    expression.accept(this);
                }
            }

            if (msgHandlerCall.getInstance() instanceof Self) {
                boolean foundMsgHandlerCall = false;
                for (int i = 0; i < msgHandlerNames.size(); i++) {
                    String[] currentMsgHandler = msgHandlerNames.get(i).split("_", 2);
                    if (currentMsgHandler[1]  == currentActorName && currentMsgHandler[0] == msgHandlerCall.getMsgHandlerName().getName() )
                        foundMsgHandlerCall = true;
                }
//                if (!foundMsgHandlerCall)
//                    errors.put("there is no msghandler name " + msgHandlerCall.getMsgHandlerName().getName() + " in actor " + currentActorName);
            }

        }

        @Override
        public void visit(Print print) {

            if (print.getArg() != null)
                print.getArg().accept(this);
        }

        @Override
        public void visit(Assign assign) {

            if (assign.getlValue() != null)
                assign.getlValue().accept(this);

            if (assign.getrValue() != null)
                assign.getrValue().accept(this);
        }
}
